<?php
session_start();					// Start the session
require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login example</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="signin.css"/>
</head>
<body>
<div class="container">
<?php 
if (!$user->isLoggedIn())		// No user is logged in
	echo $user->getLoginForm(); // Show login form
else 							// User is logged in, show some secret stuff
	echo "<p>This content is protected <a href='protected.php'>read more</a></p><a href='index.php?logout=true'>Log out</a>";
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>