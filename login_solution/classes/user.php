<?php 
/**
 * The User class handles user login/logout and maintaining of login status.
 * The class accepts a database handle in the constructor and assumes there is
 * a table called users with the columns : id, email, pwd, givenname and surename.
 * For creating new users it is also assumed that the id field is auto incrementet
 * and that the email field has an index that enforces unique email (username).
 * 
 * @author imt2291
 *
 */
class User {
	var $uid = -1;
	var $name = NULL;
	var $db;
	
	/**
	 * The constructor accepts an object of PDO that contains the database connection.
	 * It is also assumed that the session is started before this file get included.
	 * Also note that $_SESSION, $_GET and $_POST needs to be of type superglobal.
	 * 
	 * The constructor checks for a $_GET variable of logout, if it exists the user
	 * is logget out.
	 * 
	 * If $_POST['email'] is set a logon is attempted. The password should then be
	 * in $_POST['password'].
	 * 
	 * if neither $_GET['logout'] nor $_POST['email'] is set then $_SESSION['uid']
	 * is checked. If this is set the user details (name of the user) is retrieved 
	 * for the database. If no user with id $_SESSION['uid'] exists the session 
	 * variable is cleared.
	 * 
	 * @param PDO $db
	 */
	function User ($db) {
		$this->db = $db;	// Store a refence to the database connection, not needed now, but maybe in the future
		if (isset($_GET['logout'])) {	// Logging out
			unset ($_SESSION['uid']);	// Clear the session variable
		} else if (isset($_POST['email'])) {	// Logging in		
			$sql = "SELECT id, pwd, givenname, surename FROM users WHERE email=?";
			$sth = $db->prepare($sql);	
			$sth->execute (array ($_POST['email']));	// Get user info from the database
			if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {	// If user exists
				if (password_verify ($_POST['password'], $row['pwd'])) {	// If correct password
					$_SESSION['uid'] = $row['id'];		// Store user id in session variable
					$this->uid = $row['id'];			// Store user id in object
					$this->name = array ('givenname'=>$row['givenname'], 'surename'=>$row['surename']);
				} else {	// Bad password
					$this->unknownUser = 'Uknown username/password';
					// Note, never say bad password, then you confirm the user exists
				}
			} else {		// Unknow user
				$this->unknownUser = 'Uknown username/password';
				// Same as for bad password
			}
		} else if (isset($_SESSION['uid'])) {	// A user is logged in
			$sql = "SELECT  givenname, surename FROM users WHERE id=?";
			$sth = $db->prepare($sql);
			$sth->execute (array ($_SESSION['uid']));	// Find user information from the database
			if ($row = $sth->fetch()) {					// User found
				$this->uid = $_SESSION['uid'];			// Store user id in object
				$this->name = array ('givenname'=>$row['givenname'], 'surename'=>$row['surename']);
			} else {									// No such user
				unset ($_SESSION['uid']);				// Remove user id from session
			}
		}
	}

	/**
	 * Use this function to get the user id for the logged in user.
	 * Returns -1 if no user is logged in, or the id of the logged in user.
	 * 
	 * @return long integer with user id, or -1 if no user is logged in.
	 */
	function getUID() {
		return $this->uid;
	}
	
	/**
	 * Use this function to get the name of the user.
	 * Returns NULL if no user is logged in, an array with given name and 
	 * surename of a user is logged in.
	 * 
	 * @return array containing first and last name of user
	 */
	function getName() {
		return $this->name;
	}
	
	/**
	 * This method returns true if a user is logged in, false if no 
	 * user is logged in.
	 * 
	 * @return boolean value of true if a user is logged in, false if no user is logged in.
	 */
	function isLoggedIn() {
		return ($this->uid > -1);	// return true if userid > -1
	}
	
	/**
	 * This method returns HTML kode for the login form.
	 * Note that any login attempt detected by the constructor will affect
	 * the outcome of this method. If a successfull login has been performed 
	 * this method return an empty string.
	 * If a failed login has been detected an alert box will be shown informing 
	 * the user of this fact. The user name will also be filled in with information 
	 * from the failed attempt.
	 * 
	 * This method also uses the calling script as recipient in the action attribute
	 * of the form.
	 * 
	 * @return string with login form or blank string if already logged in.
	 */
	function getLoginForm () {
		if ($this->isLoggedIn())	// User is logged in
			return "";				// Return blank string.
		$alert = "";				// Initialize aler and email to blank strings
		$email = "";
		if (isset($this->unknownUser)) {	// If failed login
			// Set alert and email to be used in the form
			$alert = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> '.$this->unknownUser.'</div>';
			$email = "value='{$_POST['email']}' ";
		}
		// $_SERVER['REQUEST_URI'] points to the script that was called (and in turn included this file.)
		return '	<form class="form-signin" method="post" action="'.$_SERVER["REQUEST_URI"].'">
		<h2 class="form-signin-heading">Please sign in</h2>
		'.$alert.'
		<label for="inputEmail" class="sr-only">Email address</label>
		<input '.$email.'type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required autofocus/>
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required/>
		<div class="checkbox">
			<label>
				<input type="checkbox" value="remember-me"> Remember me
			</label>
		</div>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	</form>';
	}
	
	/**
	 * Method used to add a user to the database. Takes username, password, first and
	 * last name as parameters and attempts to add the user to the database.
	 * 
	 * On success an array with the element 'success' is returned. If it
	 * failed (probably because the username was taken) an array with 
	 * two items is return, 'error' is set and 'description' gives the reason
	 * for the failure.
	 * 
	 * @param string $uname the username of the new user
	 * @param string $pwd the password for the new user
	 * @param string $givenname the first name of the new user
	 * @param string $surename the last name of the new user
	 * @return array that indicates failure or success. 
	 */
	function addUser ($uname, $pwd, $givenname, $surename) {
		$sql = 'INSERT INTO users (email, pwd, givenname, surename) VALUES (?, ?, ?, ?)';
	    $sth = $this->db->prepare ($sql);
	    $sth->execute (array ($uname, password_hash($pwd, PASSWORD_DEFAULT), $givenname, $surename));
	    if ($sth->rowCount()==0)
	    	return (array ('error'=>'error', 'description'=>'email address already registered'));
	    $this->uid = $this->db->lastInsertId();
	    $this->name = array ('given'=>$givenname, 'sure'=>$surename);
	    return (array ('success'=>'success'));
	}
}

// Create an object of the User class, this also makes sure the constructor is called.
$user = new User($db);

