<form class="form-horizontal" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
  <div class="form-group">
    <label for="categoryname" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" value="<?php echo $this->name; ?>" name="editCategoryName" class="form-control" id="category name" placeholder="Category name">
    </div>
  </div>
  <div class="form-group">
    <label for="categorydescription" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <textarea class="form-control" name="description" id="categorydescription" placeholder="Description of category" rows="3"><?php echo $this->description; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="public" value="y" <?php 
          if ($this->public==true)	// If public then add the checked attribute
          	echo "checked='true'";
          ?>> Public category
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Store changes</button>
    </div>
  </div>
  <input type="hidden" name="id" value="<?php echo $this->selected; ?>">
</form>