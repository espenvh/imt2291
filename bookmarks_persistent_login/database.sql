-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 20, 2015 at 11:44 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bookmarks`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parentid` bigint(20) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `public` enum('y','n') COLLATE utf8_bin NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parentid`, `name`, `description`, `public`) VALUES
(1, -1, 'root', 'Root folder for user', 'n'),
(2, 1, 'Category 1', '', 'n'),
(3, 1, 'Category 2', '', 'n'),
(4, 1, 'Category 3', '', 'n'),
(5, 4, 'Category 4', '', 'n'),
(6, 3, 'Category 2.1', '', 'n'),
(7, 3, 'Category 2.2', '', 'n'),
(8, 3, 'Category 2.3', '', 'n'),
(9, 7, 'Category 2.2.1', '', 'n'),
(10, 7, 'Category 2.2.2', '', 'n'),
(11, 9, 'Category 2.2.1.1', 'Sub, sub, sub category', 'n'),
(12, 9, 'Category 2.2.1.2', 'Sub, sub, sub category', 'n'),
(13, 6, 'Category 2.1.1', 'dsfgegoøiqegeg', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `persistent_login`
--

CREATE TABLE IF NOT EXISTS `persistent_login` (
  `uid` bigint(20) NOT NULL,
  `series` char(32) COLLATE utf8_bin NOT NULL,
  `token` char(255) COLLATE utf8_bin NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`,`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` char(255) COLLATE utf8_bin NOT NULL,
  `givenname` varchar(128) COLLATE utf8_bin NOT NULL,
  `surename` varchar(128) COLLATE utf8_bin NOT NULL,
  `root` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `givenname`, `surename`, `root`) VALUES
(6, 'imt2291@hig.no', '$2y$10$om5cJOTlXwlvRyZDBgvp6.P/271vl5c8V7Zjj/hMKqxr/ABIGBOK.', 'imt2291', 'WWW-teknologi', 15);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
