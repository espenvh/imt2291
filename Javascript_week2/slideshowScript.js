﻿/**
 * Class definition and constructor for the Slideshow objects
 */
function Slideshow(name, subject) {
	// Add the CSS rules directly, makes it harder to customize but maybe we want it that way
	$(name).css ("width", "150px");
	$(name).css ("height", "150px");
	$(name).css ("overflow", "hidden");
	$(name).css ("border", "1px solid blue");
	$(name).css ("position", "relative");
	
	// A bit of fancy CSS 3 styling
	$(name).css ("border-radius", "10px");
	$(name).css ("-webkit-box-shadow", "8px 8px 30px 3px grey");
	$(name).css ("-moz-box-shadow", "8px 8px 30px 3px grey");
	$(name).css ("box-shadow", "8px 8px 30px 3px grey");

	// Put the data about the images fetched from the server in here
	this.items = Array ();
	// A pointer to the currently displayed image
	this.current = 0;
	// The id for the div tag (and also the name of the variable containing the object
	this.name = name;
	// What kind of keyword should we search for in the image library
	this.subject = subject;
	// Add the to inner div tags to the slideshow container
	$(name).append ('<div style="z-index: 2; position: absolute; top: 0px;"></div>');
	$(name).append ('<div style="z-index: 1; position: absolute; top: 0px;"></div>');
	// Fetch image data from the server
	this.fetchImages();
}

/**
 * Add the method fetchImages to the Slideshow class.
 * This method will fetch data about the images from the server
 */
Slideshow.prototype.fetchImages = function() {
	// Here, "this" means this object, ie, the object of the Slideshow class
	// When we need the reference, down in the success method of the ajax object
	// "this" will refer to the ajax object. By putting the reference to "this" into
	// "me" here we save it for later
	var me = this;
	// Reset the items array since we reload the array each time we get to the end
	this.items = Array ();
	// For the same reason we reset the pointer variable
	this.current = 0;
	// Use an Ajax call to fetch information from the server
	$.ajax({
		url: 'fetchFile.php',				// Use a proxy script
		// Fetch information from picasaweb, use the given topic as a keyword
		data: {'url':'http://picasaweb.google.com/data/feed/base/all?alt=rss&kind=photo&access=public&filter=1&q='+me.subject+'&hl=en_US'},
		dataType: 'xml',
		success: function (data) {	// This method gets called when the data is downloaded from the server
			// Each image is found in a separate item tag
			var tmp = data.getElementsByTagName("item")
			// Loop through all the images
			for (i=0; i<tmp.length; i++) {
				var item = new Object();		// An object is an easy place to store properties for the images
				// Check wheter or not to use the media: prefix
				if (tmp[i].getElementsByTagName("media:thumbnail").length>0)
					pre = "media:";
				else
					pre = "";
				// Get the image url, height and width for the image
				item.url = tmp[i].getElementsByTagName(pre+"thumbnail")[1].attributes.getNamedItem("url").value;
				item.height = tmp[i].getElementsByTagName(pre+"thumbnail")[1].attributes.getNamedItem("height").value;
				item.width = tmp[i].getElementsByTagName(pre+"thumbnail")[1].attributes.getNamedItem("width").value;
				// Store the object with the details about the image in the array in the object
				me.items[me.items.length] = item;
			}
			// Select the first div under the div containing the slideshow and hide it
			$(me.name+" > div").first().hide();
			// Start the slideshow now (all data is initialized
			me.showSlides();
		}
	});
}

/**
 * This method will be called each time its time to change the image in the slideshow.
 * The old image will fade out and the new image will fade in.
 * When we get to the end of the array of images we reload the data from the server
 * this let us always show the latest images.
 */
Slideshow.prototype.showSlides = function() {
	// Start fading out the old background image
	$(this.name+" > div").last().fadeOut ("slow");
	// Save the reference to the this object
	var me = this;
	// Start fading in the new foreground image
	$(this.name+" > div").first().fadeIn ("slow", function () {	// Once the fadein is complete
		// Set the backround image to the same as the foreground image
		$(me.name+" > div").last().html ($(me.name+" > div").first().html());
		// Show the background image
		$(me.name+" > div").last().show ();
		// Hide the foreground image
		$(me.name+" > div").first().hide ();
		
		// We are now ready to put a new foreground image in place, ready to be faded in the next time
		$(me.name+" > div").first().html (" &nbsp;");

		// This seem to happen if we manipulate the class to reload with a different 
		// subject
		if (me.current==me.items.length)
		  me.current = 0;

		// Add the image to the div tag
		$(me.name+" > div").first().append ("<img src='"+me.items[me.current].url+"'>");
		// Add a bit of css beaty ;)
		$(name+" img").css ("border-radius", "5px");
		// Position the image within the div tag
		$(me.name+" > div > img").first().css ("position", "absolute");
		// Place the image center in the div
		$(me.name+" > div > img").first().css ("top", Math.round((150-me.items[me.current].height)/2)+"px");
		$(me.name+" > div > img").first().css ("left", Math.round((150-me.items[me.current].width)/2)+"px");
	});
	// Advance the current pointer
	this.current++;
	// Check to see if we are at the end of the array
	if (this.current>this.items.length-1)
		// If at the end, fetch the image array once more from the server, gets any new images as well
		setTimeout (this.name.substring(1)+".fetchImages()", 3500);
	else
		// If not at the end, show the next image
		setTimeout (this.name.substring(1)+".showSlides()", 3500);
}
