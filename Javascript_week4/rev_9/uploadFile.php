﻿<?php
/**
 * Script used to store files saved using standard file upload dialog
 */
	session_start ();
	require_once 'db.php';
	
	if (!isset($_SESSION['user'])) {		// Just send and error message if no user is logged in
		die ('<script type="text\javascript">\nalert ("Du er ikke logget på!!!");\n</script>');
	}
	
	// SQL statement to insert a new file into the database
	$sql = 'INSERT INTO files (folderid, uid, public, name, mime, description, content, size, date)
	        VALUES (?, ?, ?, ?, ?, ?, ?, ?, now())';
	if (isset($_POST['public']))	// Should the file be marked public
		$public = 'y';
	else
		$public = 'n';
	if (is_uploaded_file($_FILES['file']['tmp_name'])) {			// Only save if an actual file is uploaded
		$content = file_get_contents($_FILES['file']['tmp_name']);
		$name = $_FILES['file']['name'];
		$mime = $_FILES['file']['type'];
		$size = $_FILES['file']['size'];
		$sth = $db->prepare ($sql);
		// Save the file
		$sth->execute (array ($_POST['folderid'], $_SESSION['user'], $public, $name, $mime, $_POST['descr'], $content, $size));
		print_r ($sth->errorInfo());		// For debuging purposes
	} else	// Give error message if no file uploaded
		die ('<script type="text\javascript">\nalert ("Ingen fil lastet opp!!!");\n</script>');
?><script type="text/javascript">
// Call the script in the window that owns this iframe
window.parent.window.fileUploaded();
//window.location = 'db.php';
</script>