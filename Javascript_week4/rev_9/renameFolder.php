﻿<?php
// We return json encoded data
header ('Content-type: application/json');

session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))			// We can't rename a folder if no user is logged in
	die (json_encode (array ('error'=>'No user logged on')));

$sql = 'SELECT * FROM folders WHERE uid=? and id=?';		// We need to get the id of the parent folder
$sth = $db->prepare ($sql);
$sth->execute (array ($_SESSION['user'], $_POST['id']));
if (!($row = $sth->fetch()))								// Parent id can not be found, aborting
	die (json_encode (array ('error'=>'Error during database operation')));
$parentid = $row['parentid'];
$sth->fetchAll();					// Reuse the statement

// Keep the parentid in the same format as the folders we will return later on
$data[] = array ('id'=>$parentid, 'name'=>'parent');

// SQL to rename the folder
$sql = 'UPDATE folders SET name=? WHERE uid=? AND id=?';
$sth = $db->prepare ($sql);
$res = $sth->execute (array ($_POST['name'], $_SESSION['user'], $_POST['id']));		// Send to the database
if ($res==0)	// No update performed, return an error message
	die (json_encode (array ('error'=>'Error during database operation')));
	
// Now we need to return a list of folders 
$sql = 'SELECT name, id, uid FROM folders WHERE uid=? and parentid=? order by name';
$sth = $db->prepare ($sql);
$sth->execute (array ($_SESSION['user'], $parentid));		// Get the list of child folders from the database
die (json_encode  (array_merge($data, $sth->fetchAll ())));	// Append the list of folder to the parent folder and return
?>