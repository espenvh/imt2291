﻿<?php
// Return json data
header ('Content-type: application/json');

session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))		// We can't rename a file if no user is logged in
	die (json_encode (array ('error'=>'No user logged on')));

// SQL statement to rename the file
$sql = 'UPDATE files SET name=? where id=? and uid=?';
$sth = $db->prepare ($sql);
$sth->execute (array($_POST['name'], $_POST['id'], $_SESSION['user']));		// Send the data to the database
echo json_encode (array('ok'=>'OK'));		// Pretend everything works each time
?>