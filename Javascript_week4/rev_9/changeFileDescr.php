﻿<?php
/** 
 * Script used to store a new file description for a file
 */

header ('Content-type: application/json');

session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))	// Send an error message if we are not logged in
	die (json_encode (array ('error'=>'No user logged on')));

// SQL statement to update the file description
$sql = 'UPDATE files SET description=? where id=? and uid=?';
$sth = $db->prepare ($sql);
// Send the statement to the database
$sth->execute (array($_POST['descr'], $_POST['id'], $_SESSION['user']));
// Return an ok message to the caller
echo json_encode (array('ok'=>'OK'));
?>