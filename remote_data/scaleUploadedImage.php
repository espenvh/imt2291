<?php
if (isset($_FILES['file'])) {		// A file is uploaded

	$maxHeight = 200;
	$maxWidth = 150;

	header ("Content-type: image/jpg");	// We will send back a jpg image

	$imgData = file_get_contents($_FILES['file']['tmp_name']);	// Read the uploaded file
	$img = imagecreatefromstring($imgData);		// Convert it to an image
	$width = imagesx($img);			// Find the width
	$height = imagesy($img);		// Fint the height
	$scaleX = $width/$maxWidth;
	$scaleY = $height/$maxHeight;
	
	$scale = ($scaleX>$scaleY)?$scaleX:$scaleY;	// What axis needs the most scaling
	$scale = ($scale>1)?$scale:1;				// Do not scale UP
	
	$scaled = imagecreatetruecolor($width/$scale, $height/$scale);	// Create new image with the scaled size

	// Scale the image
	imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);
//	header('Content-Type: '.$_FILES['file']['type']);
	header('Content-disposition: inline;filename='.$_FILES['file']['name']);	// Set the filename, when the user right clicks and says save as
	imagejpeg ($scaled, null, 100);		// Send the image to the browser
	die();		// End the script
}
?>
<form method="post" action="scaleUploadedImage.php" enctype="multipart/form-data">
<label for="file">Velg fil: </label><input type="file" id="file" name="file"/><br/>
<input type="submit" value="skaler bildet"/>
</form>