<?php
session_start();					// Start the session

require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff
require_once 'classes/categories.php';
require_once 'classes/bookmarks.php';

$pageTitle = "Welcome to the Bookmarks Database";
require_once 'include/heading.php';
?>

<div class="container">
<?php 
if ($user->getAlertStatus()!="")	// Possible unauthorized access detected
	require_once ('include/securityBreach.html');
if ($user->isLoggedIn()) {			// A user is logged in
	if (!isset($_GET['categoryID']))	// No category selected, show welcome message
		require_once ('include/welcomeUser.php');
	echo '<div class="row"><div class="col-sm-4 col-xs-12">';
	echo '<div class="panel panel-default"><div class="panel-heading">Categories</div>';
	echo '<div class="panel-body">';
	$categories->insertCategoriesTree();	// Insert category tree
	echo '</div></div>';
	echo '</div><div class="col-sm-8 col-xs-12">';
	echo '<div class="panel panel-default"><div class="panel-heading">Bookmarks</div>';
	echo '<div class="panel-body">';
	$bookmarks->insertBookmarks();			// Insert bookmark listing
	echo '</div></div>';
	echo '</div>';
	
}
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="BootstrapTreeNav/dist/js/bootstrap-treenav.min.js"></script>
</body>
</html>