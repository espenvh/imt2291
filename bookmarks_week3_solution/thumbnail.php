<?php
/**
 * This script is used to show thumbnails when adding bookmarks.
 * The script is called by using an img tag and adding the url
 * to the image as a parameter i.e. thumbnail.php?url=.....
 */
header ("Content-type: image/jpeg");
$maxHeight = 150;
$maxWidth = 150;

$content = file_get_contents($_GET['url']);	// Get the contents of the fil
$img = imagecreatefromstring($content);		// Convert string to image
$width = imagesx($img);
$height = imagesy($img);

// Find the scale factor
$scaleX = $width/$maxWidth;
$scaleY = $height/$maxHeight;

$scale = ($scaleX>$scaleY)?$scaleX:$scaleY;
$scale = ($scale>1)?$scale:1;

// Scale the image
$scaled = imagecreatetruecolor($width/$scale, $height/$scale);
imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);

// Return the scaled image to the browser
imagejpeg ($scaled, null, 100);