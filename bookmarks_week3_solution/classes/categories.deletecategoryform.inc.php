<?php 
$sql = 'SELECT count(*) as items FROM categories WHERE parentid=?';
$sth = $this->db->prepare ($sql);
$sth->execute (array($this->selected));
$row = $sth->fetch();
if ($row['items']==0) {	// No sub categories, show confirm deletion form
?>

<form class="form-horizontal" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Confirm deletion of this category</button>
    </div>
  </div>
  <input type="hidden" name="deleteid" value="<?php echo $this->selected; ?>">
</form>
<?php 
} else { // Categories has sub categories, show warning ?>	

<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
  You can not delete a category that has sub-categories!
</div>
<?php }?>