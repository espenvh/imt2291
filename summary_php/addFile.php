<?php require_once 'include/db.php'; ?>
<!DOCTYPE html>
<html>
<head>
<title>File storage</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 30px;
    min-height: 30px;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
</head>
<body>
<?php require_once 'navbar.inc.php'; ?>
<div class="container"><!-- Upload file -->
<h1>Upload file</h1>
<form class="form-horizontal" action="storeFile.php" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="description" name="description" placeholder="File description">
    </div>
  </div>
  <!-- File upload button is from 
  http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/ -->
  <div class="form-group">
    <label for="file" class="col-sm-2 control-label">File</label>
    <div class="col-sm-10">
      <div class="input-group">
        <div class="input-group-addon">
	      <span class=" btn-file">
            <span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span>
            <input type="file" name="file">
	      </span>
	    </div>
	    <input type="text" class="form-control" id="file"/>
	  </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Upload the file</button>
    </div>
  </div>
</form>
</div>
<div class="container"><!-- Upload and scale image -->
<h1>Upload and scale image</h1>
<form class="form-horizontal" action="storeImage.php" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="description" name="description" placeholder="File description">
    </div>
  </div>
  <!-- File upload button is from 
  http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/ -->
  <div class="form-group">
    <label for="file" class="col-sm-2 control-label">File</label>
    <div class="col-sm-10">
      <div class="input-group">
        <div class="input-group-addon">
	      <span class=" btn-file">
            <span class="glyphicon glyphicon-floppy-open" aria-hidden="true"></span>
            <input type="file" name="file">
	      </span>
	    </div>
	    <input type="text" class="form-control" id="imagefile"/>
	  </div>
    </div>
  </div>
  <div class="form-group">
    <label for="height" class="col-sm-2 control-label">Height</label>
    <div class="col-sm-2">
      <input type="number" class="form-control" id="height" name="height" value="150"/>
    </div>
  </div>
  <div class="form-group">
    <label for="width" class="col-sm-2 control-label">Width</label>
    <div class="col-sm-2">
      <input type="number" class="form-control" id="width" name="width" value="150"/>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Upload the file</button>
    </div>
  </div>
</form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.parent().parent().next().val(label);
});
</script>
</body>
</html>