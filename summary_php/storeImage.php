<?php
require_once 'include/db.php';
$content = file_get_contents($_FILES['file']['tmp_name']);

$maxHeight = $_POST['height'];
$maxWidth = $_POST['width'];

$img = imagecreatefromstring($content);
$width = imagesx($img);
$height = imagesy($img);
$scaleX = $width/$maxWidth;
$scaleY = $height/$maxHeight;

$scale = ($scaleX>$scaleY)?$scaleX:$scaleY;
$scale = ($scale>1)?$scale:1;

$scaled = imagecreatetruecolor($width/$scale, $height/$scale);
imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);

ob_start();
imagejpeg ($scaled, null, 100);
$scaled = ob_get_contents();
ob_end_clean();

$sql = 'INSERT INTO files(name, description, mimetype, size, content) VALUES (?, ?, ?, ?, ?)';
$sth = $db->prepare($sql);
$sth->execute(array($_FILES['file']['name'], $_POST['description'], 'image/jpeg', strlen($scaled), $scaled));
$id = $db->lastInsertId();
header("Location: fileStorage.php?uploaded=$id");