<?php
$sql = 'SELECT id, name, description, size FROM files ORDER by name';
$sth = $db->prepare ($sql);
$sth->execute (array());
while ($row = $sth->fetch(PDO::FETCH_ASSOC)) { 
	if (isset($_GET['uploaded'])&&$row['id']==$_GET['uploaded'])
		$rowClass = 'bg-success';
	else
		$rowClass = 'alternate';
	?>
	<div class="row <?php echo $rowClass; ?>">
		<div class="col-xs-10">
			<a href="download.php?id=<?php echo $row['id']; ?>"
				title="<?php echo $row['description']; ?>">
				<?php echo $row['name']?></a>
		</div>
		<div class="col-xs-2">
			<?php echo $row['size']?>
		</div>
	</div>
<?php 
}
?>
<style type="text/css">
.row.alternate:nth-child(even) {
	background-color: #eee;
}
</style>