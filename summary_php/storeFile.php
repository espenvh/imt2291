<?php
require_once 'include/db.php';
$content = file_get_contents($_FILES['file']['tmp_name']);
$sql = 'INSERT INTO files(name, description, mimetype, size, content) VALUES (?, ?, ?, ?, ?)';
$sth = $db->prepare($sql);
$sth->execute(array($_FILES['file']['name'], $_POST['description'], $_FILES['file']['type'], $_FILES['file']['size'], $content));
$id = $db->lastInsertId();
header("Location: fileStorage.php?uploaded=$id");