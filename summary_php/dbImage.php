<?php 
require_once 'include/db.php';
$sql = 'SELECT name, mimetype, content from files WHERE id=?';
$sth = $db->prepare($sql);
$sth->execute (array($_GET['id']));
if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
	header ("Content-type: ".$row['mimetype']);
	header('Content-disposition: inline; filename="'.$row['name'].'"');
	echo $row['content'];
} else {
	header ("HTTP/1.0 404 Not Found");
	echo "<h1>404 Not Found</h1>";
	echo "The file you requested could not be found.";
}
?>