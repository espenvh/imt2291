<?php
/**
 * This script is used to return the image of the user
 * get the userid from the session variable user
 */

// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';

// SQL query to get the user
$sql = 'SELECT * FROM users where uid=?';
$sth = $db->prepare ($sql);
if (!isset($_SESSION['user'])) {		// If no user is logged in, return standard avatar
	// This is a gif image
	header ('Content-type: image/gif');
	// Passthrough of the file Avatar-Head.gif
	readfile ('Avatar-Head.gif');
}
// Get the image from the database
$sth->execute (array ($_SESSION['user']));
if (($row=$sth->fetch())&&($row['img']!=null)) {	// If an image was found in the database
	// This will be a png image (we saved the scaled image as png)
	header ('Content-type: image/png');
	// Return the image data
	echo $row['img'];
} else {
	// This is a gif image
	header ('Content-type: image/gif');
	// Passthrough of the file Avatar-Head.gif
	readfile ('Avatar-Head.gif');
}
?>