<?php
session_start();
require_once 'db.php';
$sql = 'SELECT * FROM users where uid=?';
$sth = $db->prepare ($sql);
if (!isset($_SESSION['user'])) {
	header ('Content-type: image/gif');
	readfile ('Avatar-Head.gif');
}
$sth->execute (array ($_SESSION['user']));
if (($row=$sth->fetch())&&($row['img']!=null)) {
	header ('Content-type: image/png');
	echo $row['img'];
} else {
	header ('Content-type: image/gif');
	readfile ('Avatar-Head.gif');
}
?>