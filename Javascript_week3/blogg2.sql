-- phpMyAdmin SQL Dump
-- version 3.3.5
-- http://www.phpmyadmin.net
--
-- Vert: 127.0.0.1
-- Generert den: 16. Mar, 2011 11:30 AM
-- Tjenerversjon: 5.1.49
-- PHP-Versjon: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blogg2`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `entry` text NOT NULL,
  `lat` float DEFAULT NULL,
  `long` float DEFAULT NULL,
  `uid` varchar(128) DEFAULT NULL,
  `when` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `entry`
--

CREATE TABLE IF NOT EXISTS `entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `entry` text NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `when` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `entry_id` int(11) NOT NULL,
  `when` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` varchar(128) NOT NULL,
  `givenname` varchar(128) NOT NULL,
  `surename` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `pwd` char(32) NOT NULL,
  `img` mediumblob NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
