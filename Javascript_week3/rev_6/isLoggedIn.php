﻿<?php
session_start();
require_once 'db.php';
$sql = 'SELECT * FROM users WHERE uid=?';
$sth = $db->prepare ($sql);
if (!isset ($_SESSION['user']))
	die (json_encode (array ('logon'=>'NOPE')));
$sth->execute (array ($_SESSION['user']));
if ($row=$sth->fetch()) {
	echo json_encode (array ('login'=>'OK'));
} else {
	echo json_encode (array ('logon'=>'NOPE'));
}
?>