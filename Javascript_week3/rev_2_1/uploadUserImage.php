﻿<?php
/**
 * This script is used to receive a new user image (avatar image)
 * The image is scaled to dimensions of max 140x400 before being saved in the database
 * The script returns javascript code to reload the user images shown on screen.
 */

// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';

if (!isset($_SESSION['user']))	// If no user is logged in do nothing
	die ('BOBO, ikke logget på');

// Read the raw input to the script, this will be the file that was uploaded
$raw_post = file_get_contents("php://input");
	
// Set max width and height
$maxwidth = 140;
$maxheight = 400;

// Create an image object from the uploaded file
$image = imagecreatefromstring($raw_post);

// Get the width and height of the uploaded file
$width = imagesx ($image);
$height = imagesy ($image);

// Calculate the scale factor
$factor1 = $height/$maxheight;
$factor2 = $width/$maxwidth;
$factor = ($factor1>$factor2)?$factor1:$factor2;
if ($factor<1)		// We do not want to zoom in on the image
	$factor = 1;
// Find new width and height
$new_width = $width / $factor;
$new_height = $height / $factor;
// Create target image
$image_p = imagecreatetruecolor($new_width, $new_height);
// Scale the image to its new size
imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
// Start output buffering, we need to capture a png versjon of the image data
ob_start();
// Write the image as png to the output buffer
imagepng($image_p);
// Then get the content of the output buffer
$imagevariable = ob_get_contents();
// Clean up the output buffer
ob_end_clean();

// SQL statement to update the user image
$sql = 'UPDATE users SET img=? WHERE uid=?';
$sth = $db->prepare ($sql);
// Send the query to the database
$sth->execute (array ($imagevariable, $_SESSION['user']));
?>